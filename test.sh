#!/bin/bash


dune build





TEMPDIR=$(pwd)/tmp
mkdir $TEMPDIR
dune build

(
if (( $#==1 )); then
N_TOUR=$1
N_AUTHORS=5
N_POL=2
else
if (( $#==2 )); then
N_TOUR=$1
N_AUTHORS=$2
N_POL=2

else
if (( $#>=3 )); then

N_TOUR=$1
N_AUTHORS=$2
N_POL=$3

else


N_TOUR=-1
N_AUTHORS=5
N_POL=2

fi
fi
fi

    sleep 3
    for i in `seq 1 $N_AUTHORS`; do
        dune exec ./src/author.exe -- $N_TOUR>$TEMPDIR/author$i.LOG 2>$TEMPDIR/author$i.err &
    done

    sleep 10



    for i in `seq 1 $N_POL`; do
        dune exec ./src/politicien.exe -- $N_TOUR>$TEMPDIR/politicien$i.LOG  2>$TEMPDIR/politicien$i.err &
    done
) &

echo "LOG FILES in $TEMPDIR"
dune exec ./src/main_server.exe | tee $TEMPDIR/main_server.LOG

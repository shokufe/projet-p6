open Messages
open Letter
open Crypto

let max_Iter = if((Array.length Sys.argv)>0) then (int_of_string Sys.argv.(1)) else -1

let headl level st = match Consensus.head level st with
	| Some v -> v
	| None -> let _,b =  (List.nth (Hashtbl.fold (fun k v acc -> (k, v) :: acc) (Store.get_word_table st) []) ((Hashtbl.hash level) mod (Store.length st))) in b;;


let make_letter_on_hash sk pk level head_hash letter : letter =
  Letter.make ~letter ~head:head_hash ~level ~pk ~sk

let make_letter_on_block sk pk level block letter : letter =
  let head_hash = hash block in
  make_letter_on_hash sk pk level head_hash letter
  
   let rec cdto_list st = match st with
 | [] -> []
 | (_,b)::c -> (b)::(cdto_list c);;
 
 let to_listl st = Hashtbl.fold (fun k v acc -> (k, v) :: acc) st []

let random_char () = (Char.chr (97 + (Random.int 26)))

let messageper (letter:Letter.t) = Messages.Inject_letter letter

let send_new_letter sk pk level store head=
	ignore store;
  (* Get blockchain head *)
    (* Create new random letter *)
      let letter =
        make_letter_on_block
          sk
          pk
          level
          (Word.to_bigstring head)
          (random_char ())
      in
      (* Send letter *)
      let message = messageper letter in
      Client_utils.send_some message

let run ?(max_iter = 0) () =
  (* Generate public/secret keys *)
  let (pk, sk) = Crypto.genkeys () in

  (* Register to the server *)
  let reg_msg = Messages.Register pk in
  Client_utils.send_some reg_msg ;

  (* drop provided letter_bag *)
  ( match Client_utils.receive () with
  | Messages.Letters_bag _ -> ()
  | _ -> assert false ) ;

  (* Get initial wordpool *)
  let getpool = Messages.Get_full_wordpool in
  Client_utils.send_some getpool ;
  let wordpool =
    match Client_utils.receive () with
    | Messages.Full_wordpool wordpool -> wordpool
    | _ -> assert false
  in

  (* Generate initial blocktree --- set of valid words *)
  let store = Store.init_words () in
  let level = ref wordpool.current_period in
  Store.add_words store wordpool.words !level;

  (* Create and send first letter *)
  send_new_letter sk pk wordpool.current_period store (headl (!level - 1) store) ;

  (* start listening to server messages *)
  Client_utils.send_some Messages.Listen ;
  (* start main loop *)
  let s = ref (headl (!level - 1) store) in
  let rec loop max_iter =
    if !level = max_iter then ()
    else (
      ( match Client_utils.receive () with
      | Messages.Inject_word w -> Store.add_word store !level w ;
          Option.iter
            (fun head ->
              if head = w then ( 
                Log.log_info "Head updated to incoming word %a@." Word.pp w ;
                Store.add_word store !level w;s:=w
                 )
              else Log.log_info "incoming word %a not a new head@." Word.pp w)
            (Consensus.head (!level - 1) store)
      | Messages.Next_turn p -> level := p;
                send_new_letter sk pk !level store !s
      | Messages.Inject_letter _ | _ -> () ) ;
      loop max_iter  )
  in
  loop max_iter; Consensus.fitness (cdto_list (to_listl (Store.get_word_table (Store.add_words store wordpool.words !level;store)))) 0

let rec afficher_score scores = match scores with
	| [] -> ()
	| (a,b)::c -> Log.log_info "Le politicien ayant pour clé %s a eu un score de %d." a b;afficher_score c;; 

let _ =
  let main =
    Random.self_init () ;
    let () = Client_utils.connect () in
    (afficher_score (run ~max_iter:(max_Iter) ()))
  in
  let l = main in
  (*Une fois la partie términé les auteurs assignent les points aux politiciens.*)
  l
  

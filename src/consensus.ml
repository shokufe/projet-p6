open Constants

(* ignoring unused variables - to be removed *)
let _ = ignore genesis

module Scorep = Map.Make(String)

(* end ignoring unused variables - to be removed *)


let get_stringk pk = Bigstring.to_string (Crypto.pk_to_bigstring pk)

let point_letter l = match l with
	| 'a' -> 1
	| 'b' -> 3
	| 'c' -> 3
	| 'd' -> 2
	| 'e' -> 1
	| 'f' -> 4
	| 'g' -> 2
	| 'h' -> 4
	| 'i' -> 1
	| 'j' -> 8
	| 'k' -> 10
	| 'l' -> 1
	| 'm' -> 2
	| 'n' -> 1
	| 'o' -> 1
	| 'p' -> 3
	| 'q' -> 8
	| 'r' -> 1
	| 's' -> 1
	| 't' -> 1
	| 'u' -> 1
	| 'v' -> 4
	| 'w' -> 10
	| 'x' -> 10
	| 'y' -> 10
	| 'z' -> 10
	| f -> Log.log_error "Erreur le caractere %c n'est pas reconnue en tant que lettre" f;0;;

let rec word_score (word:(char list)) : int =
  match word with
  | [] -> 0
  | a::b -> (point_letter a) + (word_score b);;

let rec dto_listl (st:(Letter.t list)) = match st with
 | [] -> []
 | b::c -> (Char.lowercase_ascii (b.letter))::(dto_listl c);;

let mupdate scorep pk s = match Scorep.find_opt (get_stringk pk) scorep with
	| Some v -> Scorep.add (get_stringk pk) (v+s) scorep
	| None -> Scorep.add (get_stringk pk) s scorep;;
	
let rec word_scorel (letters:Letter.t list) scorep =
  match letters with
  | [] -> scorep
  | a::b -> (word_scorel b (mupdate scorep a.author (point_letter a.letter)))  ;;	

(*mode 0 -> on renvoie le score des politiciens, des auteurs sinon.*)
let rec sfitness scorep (word:Word.t list) mode = match word with
	| [] -> scorep
	| a::b -> let s = word_score (dto_listl a.word ) in 
	if(mode=0) then (sfitness (mupdate scorep a.politician s) b mode) else (sfitness (word_scorel a.word scorep) b mode);;(*TEST d'abord seulement les politiciens*)

let fitness (word:(Word.t list)) (mode:int) = Scorep.bindings (sfitness Scorep.empty word mode)

(* TODO *)



let head level (st : Store.word_store) = (Hashtbl.find_opt (Store.get_word_table st)(Crypto.hash(Bigstring.of_string (Int.to_string level))));

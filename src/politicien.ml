(* open Messages *)
open Word
open Letter


let max_Iter = if((Array.length Sys.argv)>0) then (int_of_string Sys.argv.(1)) else -1

type politician = { sk : Crypto.sk; pk : Crypto.pk } [@@deriving yojson, show]

let headl level st = match Consensus.head level st with
	| Some v -> v
	| None ->  let _,b =  (List.nth (Hashtbl.fold (fun k v acc -> (k, v) :: acc) (Store.get_word_table st) []) ((Hashtbl.hash level) mod (Store.length st))) in b ;;
	
type state = {
  politician : politician;
  word_store : Store.word_store;
  letter_store : Store.letter_store;
  next_words : word list;
}

let make_word_on_hash level letters politicianp head_hash : word =
  let head = head_hash in
  let politician = politicianp.pk in
  let sk = politicianp.sk in
  let word = letters  in
  Word.make_p ~word ~head ~politician ~level sk
  



let make_word_on_blockletters level letters politician head : word =
  let head_hash = hash head in
  make_word_on_hash level letters politician head_hash

  
(*convertie une liste de letters en mot*)
let rec letters_to_mot lt = match lt with
	| [] -> ""
	| a::b -> let _, (m:letter) = a in (String.lowercase_ascii (Char.escaped m.letter))^(letters_to_mot b);;

(*verifie si un mot est contenue dans une liste*)
let rec included_string l el = match l with
	| [] -> false
	| a::b -> if(a=el) then true else included_string b el;;

(*Credit for this code goes to http://typeocaml.com/2015/05/05/permutation/ *)

let rec print_list l = match l with
	[] -> ()
	| a::b -> print_string a; print_list b;;
	
let rec print_listl l = match l with
	[] -> ()
	| a::b -> print_list a; print_string "*"; print_listl b;; 

type direction = L | R

let attach_direction a = Array.map (fun x -> (x, L)) a

let swap a i j = let tmp = a.(j) in a.(j) <- a.(i); a.(i) <- tmp

let is_movable a i =  
  let x,d = a.(i) in
  match d with
    | L -> if i > 0 && x > (fst a.(i-1)) then true else false
    | R -> if i < Array.length a - 1 && x > (fst a.(i+1)) then true else false

let move a i =  
  let _,d = a.(i) in
  if is_movable a i then 
    match d with
      | L -> swap a i (i-1)
      | R -> swap a i (i+1)
  else
    failwith "not movable"
    
let scan_movable_largest a =  
  let rec aux acc i =
    if i >= Array.length a then acc
    else if not (is_movable a i) then aux acc (i+1)
    else
      let x,_ = a.(i) in
      match acc with
        | None -> aux (Some i) (i+1)
        | Some j -> aux (if x < fst(a.(j)) then acc else Some i) (i+1)
  in
  aux None 0
  
let flip = function | L -> R | R -> L

let scan_flip_larger x a =  
  Array.iteri (fun i (y, d) -> if y > x then a.(i) <- y,flip d) a

let permutations_generator l =  
  let a = Array.of_list l |> attach_direction in
  let r = ref (Some l) in
  let next () = 
    let p = !r in
    (match scan_movable_largest a with (* find largest movable *)
      | None -> r := None (* no more permutations *)
      | Some i -> 
        let x, _ = a.(i) in (
        move a i; (* move *)
        scan_flip_larger x a; (* after move, scan to flip *)
        r := Some (Array.map fst a |> Array.to_list)));
    p
  in
  next;;
  

  


(*Credit for this page goes to http://typeocaml.com/2015/05/05/permutation/ *)  

(*convertie un hashtbl en liste *)
let to_list st = Hashtbl.fold (fun k v acc -> (k, v) :: acc) (Store.get_letter_table st) []

let to_listl st = Hashtbl.fold (fun k v acc -> (k, v) :: acc) st []

let rec dto_list st = match st with
 | [] -> []
 | (_,b)::c -> b::(dto_list c);;
 
 let rec cdto_list st = match st with
 | [] -> []
 | (_,b)::c -> (b)::(cdto_list c);;

let rec find_word_s gen dic = match (gen()) with
	| None -> Log.log_info "Aucun mot retrouvé sur cet enesemble de mot."; []
	| Some b -> if(included_string dic (letters_to_mot b)) then b else (find_word_s gen dic);;
	
let find_word lv dic = find_word_s (permutations_generator lv) dic;;

(*On ajoute la liste des lettres*)
let send_new_wordl (st:state) (level:int) (ll : Letter.t List.t) head=
  (* generate a word above the blockchain head, with the adequate letters *)
  let new_word = (make_word_on_blockletters level ll st.politician head) in 
  (* then send it to the server *)
  let message = Messages.Inject_word new_word in
   let t = Client_utils.send message in match t with 
   	| Some _ -> Log.log_info "L'envoi niveau %d a reussi." level
   	| None -> Log.log_error "L'envoi niveau %d a echoué." level;;
   	
   	
  
let new_key sk pk = {sk;pk}
let new_state politician word_store letter_store next_words = {politician;word_store;letter_store;next_words}

let run ?(max_iter = 0) () =

  (* Generate public/secret keys *)
  let (pk, sk) = Crypto.genkeys ()  in 
  
  
  
  (* Get initial wordpool *)
  let getpool = Messages.Get_full_wordpool in
  Client_utils.send_some getpool ;
  let wordpool =
    match Client_utils.receive () with
    | Messages.Full_wordpool wordpool -> wordpool
    | _ -> assert false
  in 
  
  (* Generate initial blocktree *)
  let store = Store.init_words () in
  let level = ref wordpool.current_period in
  Store.add_words store wordpool.words !level;

  (* Get initial letterpool *)
  let getpoolletter = Messages.Get_full_letterpool in 
  Client_utils.send_some getpoolletter ;
  let letterpool =
    match Client_utils.receive () with
    | Messages.Full_letterpool letterpool -> letterpool
    | _ -> assert false
  in 
  
  (* Generate initial letterpool*)
  let storel = Store.init_letters () in
  Store.add_letters storel letterpool.letters ;
  let lvide = ref storel in
  let statep = (new_state (new_key sk pk) store storel []) in
  
  (* Create and send first word *)
  send_new_wordl statep wordpool.current_period (dto_list (to_list storel)) (headl (!level - 1) store)  ;
  
  (* start listening to server messages *)
  Client_utils.send_some Messages.Listen ;
  
  (*  main loop *)
  let s = ref (headl (!level - 1) store) in
  let rec loop max_iter =
    if !level = max_iter then ()
    else( 
      ( match Client_utils.receive () with
      | Messages.Inject_word nw -> Log.log_info "lvide = %d caractere ----" (Store.lengthl !lvide); (
          Option.iter
            (fun head ->
              if head = nw then (
                Log.log_info "Head updated to incoming word %a@." Word.pp nw ;Store.add_word store !level nw;
                s:=head
                 )
              else Log.log_info "incoming word %a not a new head@." Word.pp nw)
            (Consensus.head (!level - 1) store)) ;Store.add_letters !lvide letterpool.letters
      | Messages.Next_turn p -> level := p;Log.log_info "Tour numero %d." !level
      		(*Si aucun mot possible ne rien faire!*)
      		
      | Messages.Inject_letter l -> Store.add_letter !lvide l ;
            let ch = (if((Store.lengthl !lvide)<8) then
      		  (find_word (to_list !lvide) (Client_utils.list_of_dict "./dict/dict_100000_1_10.txt")) else (if((Store.lengthl !lvide)<15) then
      		  (find_word (to_list !lvide) (Client_utils.list_of_dict "./dict/dict_100000_5_15.txt")) else (if((Store.lengthl !lvide)<60) then
      		  (find_word (to_list !lvide) (Client_utils.list_of_dict "./dict/dict_100000_25_75.txt")) else (if((Store.lengthl !lvide)<200) then
      		  (find_word (to_list !lvide) (Client_utils.list_of_dict "./dict/dict_100000_50_200.txt")) else
      		  ((Log.log_error("Trop de lettre comprisent dans l'ensemble!\n");[]) ))))) in 
      		  (if((List.length ch)>0) then (send_new_wordl statep !level (dto_list ch) !s) else (Log.log_error "R--ien trouvé pour %d caracteres." (Store.lengthl !lvide) ))
      
      | _ -> Log.log_error "Message non reconnue."  )
      	;loop max_iter)
  in
  loop (max_iter-1); Consensus.fitness (cdto_list (to_listl (Store.get_word_table store)) ) 1

let _ =
  let main =
    Random.self_init () ;
    let () = Client_utils.connect () in
    run ~max_iter:(max_Iter) ()
  in
  let l = main in
  (*Une fois la partie terminé les politciens assignent les points aux auteurs*)
  l 
  
  
